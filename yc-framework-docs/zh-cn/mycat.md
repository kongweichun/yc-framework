# 分库分表之MyCat

MyCat官网:
http://www.mycat.org.cn/

MyCat文档(支持在线阅读):
http://www.mycat.org.cn/document/mycat-definitive-guide.pdf

MyCat源代码:
https://github.com/MyCATApache/Mycat-Server

MyCat问题列表:
https://github.com/MyCATApache/Mycat-Server/issues

博客文章:
[深入浅出分库分表之MyCat](https://youcongtech.com/2022/04/16/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BA%E5%88%86%E5%BA%93%E5%88%86%E8%A1%A8%E4%B9%8BMyCat/)